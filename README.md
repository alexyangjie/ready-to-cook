# Ready to Cook
This is a cross-platform app that helps you prepare materials for cooking.

Version: 0.1.0

Phase 1 will build the iOS and Android app.  
Phase 2 will support backup and syncing.  
Phase 3 will support more platforms.  

Clone the project and then navigate to the readytocook folder to run `npm install`

To run the app in mock, run `ionic serve`

To run the app in simulator, run `ionic emulate ios` or `ionic emulate android`

To run the app in real device, run `ionic run android` with phone plugged in or `ionic prepare ios` and then open the platform/ios in XCode
