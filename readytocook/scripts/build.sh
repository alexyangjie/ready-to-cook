#!/bin/sh
# setup variables
export APP_NAME="water-storage"
export PATH="$PATH:/usr/local/bin"
export SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
export WORKSPACE=$SCRIPT_DIR/..
export IOS_ARCHIVE=$WORKSPACE/temp/ios/archive/water-storage.xcarchive
export IOS_DIST_DIR=$WORKSPACE/dist/ios
export IOS_WORKSPACE=$WORKSPACE/platforms/ios/'Water Storage.xcworkspace'
export EXPORT_OPTIONS=$WORKSPACE/exportOptions.plist

echo Water Storage App Build Script
echo APP_NAME: $APP_NAME
echo PATH: $PATH
echo SCRIPT_DIR: $SCRIPT_DIR
echo WORKSPACE: $WORKSPACE
echo IOS_ARCHIVE: $IOS_ARCHIVE
echo IOS_DIST_DIR: $IOS_DIST_DIR
echo IOS_WORKSPACE: $IOS_WORKSPACE
echo EXPORT_OPTIONS: $EXPORT_OPTIONS

# go to workspace
cd $WORKSPACE

# install tools
npm install

# update platforms
ionic platform add ios

# prepare code for xcode build
ionic prepare ios

# build ios
xcodebuild -workspace "$IOS_WORKSPACE" -scheme 'Water Storage' -configuration 'Release' clean archive -archivePath $IOS_ARCHIVE
xcodebuild -exportArchive -archivePath $IOS_ARCHIVE -exportOptionsPlist $EXPORT_OPTIONS -exportPath $IOS_DIST_DIR
