import {Pipe, PipeTransform} from "@angular/core";

/**
 * keys pipe that can extract the keys of an object
 */
@Pipe({name: 'keys'})
export class KeysPipe implements PipeTransform {
  transform(value, args:string[]) : any {
    let keys = [];
    for (let key in value) {
      keys.push({key: key, value: value[key]});
    }
    return keys;
  }
}

@Pipe({name: 'decimal'})
export class DecimalPipe implements PipeTransform {
  transform(value, args:string[]) : any {
    let result = '';
    if (value !== undefined && value !== null) {
      result = value.toFixed(1);
    }
    return result;
  }
}
