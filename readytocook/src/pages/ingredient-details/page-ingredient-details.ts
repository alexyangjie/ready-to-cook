import { Component } from '@angular/core';
import { Validators, FormBuilder } from '@angular/forms';
import { Events, NavParams, NavController } from 'ionic-angular';
import { ConfigService } from '../../providers/config-service';
import { LogService } from '../../providers/log-service';
import { IngredientsService } from '../../providers/ingredients-service';
import { Ingredient } from '../../models/model-ingredient';
import moment from 'moment';

@Component({
  selector: 'page-ingredient-details',
  templateUrl: 'page-ingredient-details.html'
})
export class IngredientDetailsCtrl {

  private className = 'IngredientDetailsCtrl -> ';

  ingredient: Ingredient;
  ingredientName: string;
  lastUpdate: string;
  ingredientForm: any;

  constructor(public navParams: NavParams,
              public navCtrl: NavController,
              public ingredientsService: IngredientsService,
              public logger: LogService,
              public events: Events,
              public configService: ConfigService,
              public formBuilder: FormBuilder) {
    this.ingredientName = navParams.get('ingredientName');
  }

  ionViewDidLoad() {
    this.ingredientsService.init()
      .then(() => {
        if (this.ingredientName && this.ingredientName.length > 0) {
          this.ingredient = this.ingredientsService.getIngredient(this.ingredientName);
        } else {
          this.ingredient = {
            name: '',
            quantity: null,
            unit: ''
          };
        }
        this.lastUpdate = moment(this.ingredientsService.lastUpdate).local().format('DD/MM/YYYY HH:mm:ss');

        // create form
        this.ingredientForm = this.formBuilder.group({
          name: [this.ingredient.name, Validators.required],
          quantity: [this.ingredient.quantity, Validators.pattern('([1-9][0-9]*|[0-9])(\.[0-9]{1,2})?')],
          unit: [this.ingredient.unit, Validators.required]
        });
      });
  }

  updateIngredient() {
    let methodName = this.className + 'updateIngredient -> ';
    this.logger.info(methodName, 'form data: ' + JSON.stringify(this.ingredientForm.value));
    if (this.isIngredientValid(this.ingredientForm.value)) {
      this.ingredientsService.updateIngredient(this.ingredientForm.value);
      this.events.publish('ingredients:updated');
      this.navCtrl.pop();
    }
  }

  isIngredientValid(ingredient: Ingredient): boolean {
    let methodName = this.className + 'isIngredientValid -> ';
    this.logger.info(methodName, 'ingredient to be updated: ' + JSON.stringify(ingredient));
    return ingredient.name && ingredient.name.length > 0 && ingredient.unit && ingredient.unit.length > 0;
  }
}
