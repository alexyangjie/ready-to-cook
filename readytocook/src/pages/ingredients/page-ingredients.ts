import { Component } from '@angular/core';
import { LogService } from '../../providers/log-service';
import { ConfigService } from '../../providers/config-service';
import { Events, NavController, ItemSliding, AlertController } from 'ionic-angular';
import { IngredientsService } from '../../providers/ingredients-service';
import { IngredientDetailsCtrl } from '../ingredient-details/page-ingredient-details';
import { Ingredient } from '../../models/model-ingredient';

import moment from 'moment';

@Component({
  selector: 'page-ingredients',
  templateUrl: 'page-ingredients.html'
})
export class IngredientsCtrl {

  private className: string = 'IngredientsCtrl -> ';

  ingredients: Array<Ingredient>;
  lastUpdate: string;

  constructor(public navCtrl: NavController,
              public alertCtrl: AlertController,
              public logger: LogService,
              public configService: ConfigService,
              public events: Events,
              public ingredientsService: IngredientsService) {
    this.events.subscribe('ingredients:updated', () => {
      this.ingredients = this.ingredientsService.ingredients;
      this.lastUpdate = moment(this.ingredientsService.lastUpdate).local().format('DD/MM/YYYY HH:mm:ss');
    });
  }

  ionViewDidLoad() {
    this.ingredientsService.init()
      .then(() => {
        this.ingredients = this.ingredientsService.ingredients;
        this.lastUpdate = moment(this.ingredientsService.lastUpdate).local().format('DD/MM/YYYY HH:mm:ss');
      });
  }

  removeIngredient(ingredientName: string, itemSliding?: ItemSliding) {
    let methodName = this.className + 'removeIngredient ->';
    this.logger.log(methodName, 'Remove ingredient ' + ingredientName);

    let confirm = this.alertCtrl.create({
      title: 'Remove an ingredient',
      message: this.configService.appConfig.messages.WARNING_INGREDIENT_DELETE,
      buttons: [
        {
          text: 'No',
          handler: () => {
            this.logger.log(methodName, 'Remove cancelled. No action taken');
            if (itemSliding) {
              itemSliding.close();
            }
          }
        },
        {
          text: 'Yes',
          handler: () => {
            this.logger.log(methodName, 'User confirmed removal.');
            this.ingredientsService.removeIngredient(ingredientName);
            this.ingredients = this.ingredientsService.ingredients;
          }
        }
      ]
    });
    confirm.present();
  }

  showDetails(ingredientName?: string, itemSliding?: ItemSliding) {
    let methodName = this.className + 'showDetails -> ';
    if (itemSliding) {
      itemSliding.close();
    }
    this.logger.log(methodName, 'Editing ingredient details for ' + ingredientName);
    this.navCtrl.push(IngredientDetailsCtrl, {
      ingredientName: ingredientName
    });
  }
}


