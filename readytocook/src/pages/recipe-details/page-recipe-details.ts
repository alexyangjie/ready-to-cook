import { Component } from '@angular/core';
import { Validators, FormBuilder } from '@angular/forms';
import { Events, NavParams, NavController } from 'ionic-angular';
import { RecipesService } from '../../providers/recipes-service';
import { IngredientsService } from '../../providers/ingredients-service';
import { ConfigService } from '../../providers/config-service';
import { LogService } from '../../providers/log-service';
import { Recipe } from '../../models/model-recipe';
import { Ingredient } from '../../models/model-ingredient';

import moment from 'moment';
import lodash from 'lodash';

@Component({
  selector: 'page-recipe-details',
  templateUrl: 'page-recipe-details.html'
})
export class RecipeDetailsCtrl {

  private className = 'RecipeDetailsCtrl -> ';

  recipe: Recipe;
  recipeName: string;
  ingredientNameList: Array<any>;
  ingredientDefinition: any = {};
  ingredientForms: Array<Ingredient> = [];
  lastUpdate: string;
  recipeForm: any;

  constructor(public navParams: NavParams,
              public navCtrl: NavController,
              public recipesService: RecipesService,
              public ingredientsService: IngredientsService,
              public logger: LogService,
              public events: Events,
              public configService: ConfigService,
              public formBuilder: FormBuilder) {
    this.recipeName = navParams.get('recipeName');
  }

  ionViewDidLoad() {
    let methodName = this.className + 'ionViewDidLoad';
    Promise.all([
      this.recipesService.init(),
      this.ingredientsService.init()]
    ).then(() => {
      this.ingredientNameList = lodash.map(this.ingredientsService.ingredients, 'name');
      lodash.each(this.ingredientsService.ingredients, (ingredient) => {
        this.ingredientDefinition[ingredient.name] = ingredient.unit;
      });
      this.logger.log(methodName, 'ingredientDefinition -> ' + JSON.stringify(this.ingredientDefinition));
      if (this.recipeName && this.recipeName.length > 0) {
        this.recipe = this.recipesService.getRecipe(this.recipeName);
      } else {
        this.recipe = {
          name: '',
          procedures: '',
          ingredients: []
        };
      }
      this.lastUpdate = moment(this.recipesService.lastUpdate).local().format('DD/MM/YYYY HH:mm:ss');

      // create form
      this.recipeForm = this.formBuilder.group({
        name: [this.recipe.name, Validators.required],
        procedures: [this.recipe.procedures, Validators.required]
      });

      // create ingredient forms
      lodash.each(this.recipe.ingredients, (ingredient) => {
        let ingredientForm: Ingredient = {
          name: ingredient.name,
          unit: ingredient.unit,
          quantity: ingredient.quantity,
        };
        this.ingredientForms.push(ingredientForm);
      });
    });
  }


  updateRecipe() {
    let methodName = this.className + 'updateRecipe';
    this.logger.info(methodName, 'form data: ' + JSON.stringify(this.recipeForm.value));
    this.logger.info(methodName, 'ingredients number: ' + this.ingredientForms.length);

    // autofill the unit for existing ingredients
    lodash.each(this.ingredientForms, (ingredientForm) => {
      if (this.ingredientDefinition[ingredientForm.name]) {
        ingredientForm.unit = this.ingredientDefinition[ingredientForm.name];
      }
    });

    // map new recipe values
    let updatedRecipe: Recipe = {
      name: this.recipeForm.value.name,
      procedures: this.recipeForm.value.procedures,
      ingredients: this.ingredientForms
    };

    // save new recipe
    if (this.isRecipeValid(updatedRecipe)) {
      this.recipesService.addOrUpdateRecipe(updatedRecipe);
      this.events.publish('recipes:updated');
      this.navCtrl.pop();
    }
  }


  addIngredient() {
    let newIngredient: Ingredient = {
      name: '',
      unit: '',
      quantity: null
    };
    this.ingredientForms.push(newIngredient);
    console.log(this.ingredientForms);
  }

  removeIngredient(i: number) {
    let methodName = this.className + 'removeIngredient';
    this.logger.log(methodName, 'Remove ingredient #' + i);
    lodash.pullAt(this.ingredientForms, [i]);
  }

  removeRecipe() {
    this.recipesService.removeRecipe(this.recipeName);
    this.events.publish('recipes:updated');
    this.navCtrl.pop();
  }

  isRecipeValid(recipe: Recipe): boolean {
    let methodName = this.className + 'isRecipeValid -> ';
    this.logger.info(methodName, 'ingredient to be updated: ' + JSON.stringify(recipe));
    return recipe.name && recipe.name.length > 0
      && recipe.procedures && recipe.procedures.length > 0
      && recipe.ingredients && recipe.ingredients.length > 0;
  }
}
