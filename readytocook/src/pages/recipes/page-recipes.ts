import { Component } from '@angular/core';
import { Events, NavController, Platform } from 'ionic-angular';
import { LogService } from '../../providers/log-service';
import { RecipesService } from '../../providers/recipes-service';
import { RecipeDetailsCtrl } from '../recipe-details/page-recipe-details';
import { Recipe } from '../../models/model-recipe';

import moment from 'moment';

@Component({
  selector: 'page-recipes',
  templateUrl: 'page-recipes.html'
})
export class RecipesCtrl {

  private className: string = 'RecipesCtrl -> ';

  recipes: Array<Recipe>;
  lastUpdate: string;

  constructor(public navCtrl: NavController,
              public logger: LogService,
              public events: Events,
              public recipesService: RecipesService,
              public platform: Platform) {
    this.events.subscribe('recipes:updated', () => {
      this.recipes = this.recipesService.recipes;
      this.lastUpdate = moment(this.recipesService.lastUpdate).local().format('DD/MM/YYYY HH:mm:ss');
    });
  }

  ionViewDidLoad() {
    let methodName = this.className + 'ionViewDidLoad -> ';
    this.logger.log(methodName, 'Page loaded.');
    this.platform.ready().then(() => {
      this.logger.log(methodName, 'Platform ready now');
      this.recipesService.init()
        .then(() => {
          this.recipes = this.recipesService.recipes;
          this.lastUpdate = moment(this.recipesService.lastUpdate).local().format('DD/MM/YYYY HH:mm:ss');
        });
    });
  }


  showDetails(recipeName?: string) {
    this.navCtrl.push(RecipeDetailsCtrl, {
      recipeName: recipeName
    });
  }
}


