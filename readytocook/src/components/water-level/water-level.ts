import { Component, Input } from '@angular/core';
import { WaterLevelInfo } from '../../models/water-level-info';

@Component({
  selector: 'water-level',
  templateUrl: 'water-level.html'
})
export class WaterLevelComponent {

  @Input()
  waterLevelInfo: WaterLevelInfo;

  @Input()
  period: string;

  constructor() {}
}
