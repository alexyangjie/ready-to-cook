export interface Ingredient {
  name: string,
  unit: string,
  quantity: number,
  image?: string
}
