import { Ingredient } from './model-ingredient';

export interface Recipe {
  name: string,
  procedures: string,
  image?: string,
  ingredients: Array<Ingredient>
}
