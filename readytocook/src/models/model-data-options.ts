export interface DataOptions {
  remoteUrl?: string,
  dataKey: string,
  validationElement?: string
}
