export interface WaterLevelInfo {
  name: string,
  thisDate: string,
  levelTodayGL: number,
  levelTodayPercent: number,
  levelYesterdayGL: number,
  levelYesterdayPercent: number,
  changeYesterdayPercent: number,
  levelLastYearGL: number,
  levelLastYearPercent: number,
  changeLastYearPercent: number,
  rainfallMM?: number,
  url?: string
}
