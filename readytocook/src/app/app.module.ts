// angular core libraries
import { NgModule, ErrorHandler } from '@angular/core';
import { HttpModule } from '@angular/http';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';

// plugins
import { Storage } from '@ionic/storage';
import '../../node_modules/chart.js/dist/Chart.min.js';
import { ChartsModule } from 'ng2-charts/ng2-charts';

// app services
import { AppService } from '../providers/app-service';
import { ConfigService } from '../providers/config-service';
import { LogService } from '../providers/log-service';
import { DataService } from '../providers/data-service';
import { RecipesService } from "../providers/recipes-service";
import { IngredientsService } from '../providers/ingredients-service';

// pipes
import { KeysPipe } from '../modules/pipes';
import { DecimalPipe } from '../modules/pipes';

// pages
import { RecipesCtrl } from '../pages/recipes/page-recipes';
import { RecipeDetailsCtrl } from '../pages/recipe-details/page-recipe-details';
import { IngredientsCtrl } from '../pages/ingredients/page-ingredients';
import { IngredientDetailsCtrl } from '../pages/ingredient-details/page-ingredient-details';

// components


// app bootstrap
import { MyApp } from './app.component';

@NgModule({
  declarations: [
    MyApp,
    RecipesCtrl,
    RecipeDetailsCtrl,
    IngredientsCtrl,
    IngredientDetailsCtrl,
    KeysPipe,
    DecimalPipe
  ],
  imports: [
    IonicModule.forRoot(MyApp),
    ChartsModule,
    HttpModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    RecipesCtrl,
    RecipeDetailsCtrl,
    IngredientsCtrl,
    IngredientDetailsCtrl
  ],
  providers: [
    Storage,
    AppService,
    ConfigService,
    LogService,
    DataService,
    RecipesService,
    IngredientsService,
    {provide: ErrorHandler, useClass: IonicErrorHandler}]
})
export class AppModule {}
