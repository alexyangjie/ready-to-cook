import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, Events, LoadingController, ToastController, AlertController } from 'ionic-angular';
import { StatusBar, Splashscreen } from 'ionic-native';
import { AppService } from '../providers/app-service';
import { ConfigService } from '../providers/config-service';
import { DataService } from '../providers/data-service';
import { RecipesCtrl } from '../pages/recipes/page-recipes';
import { IngredientsCtrl } from '../pages/ingredients/page-ingredients';
import { DataOptions } from '../models/model-data-options';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = RecipesCtrl;

  pages: Array<{title: string, component: any}>;

  constructor(public platform: Platform,
              public events: Events,
              public loadingCtrl: LoadingController,
              public alertCtrl: AlertController,
              public appService: AppService,
              public dataService: DataService,
              public configService: ConfigService,
              public toastCtrl: ToastController) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Recipes', component: RecipesCtrl },
      { title: 'Ingredients', component: IngredientsCtrl }
    ];

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      StatusBar.styleDefault();
      Splashscreen.hide();
      this.registerEvents();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }

  registerEvents() {
    this.events.subscribe('data:updated', (dataOptions: DataOptions, value: any) => {
      this.dataService.update(dataOptions, value);
    });

    this.events.subscribe('error:connection', () => {
      this.presentToast(this.configService.appConfig.messages.ERROR_CONNECTION);
    });

    this.events.subscribe('error:app', () => {
      this.presentAlert('Error in app initialization', this.configService.appConfig.messages.ERROR_GENERIC);
    });
  }

  presentToast(message: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
      position: 'bottom'
    });
    toast.present();
  }

  presentAlert(title:string, content: string) {
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: content,
      buttons: ['OK']
    });
    alert.present();
  }
}
