"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var lodash_1 = require('lodash');
require('rxjs/add/operator/map');
var DataService = (function () {
    function DataService(http, logger, storage) {
        this.http = http;
        this.logger = logger;
        this.storage = storage;
        this.className = 'DataService -> ';
        console.log('Hello DataService Provider');
    }
    /**
     * Refresh the data store with new data from backend, whether is is from local mock data or online feed
     * @param dataOptions the options for data service
     * @returns a Promise which resolves to the actual data fetched or the error
     */
    DataService.prototype.refresh = function (dataOptions) {
        var _this = this;
        var methodName = this.className + 'refresh';
        var url;
        return new Promise(function (resolve, reject) {
            if (core_1.isDevMode()) {
                url = 'assets/mock-data/' + dataOptions.dataKey + '.json';
            }
            else {
                url = dataOptions.remoteUrl;
            }
            _this.logger.debug(methodName, 'Getting data from url: ' + url);
            _this.http.get(url)
                .map(function (res) {
                return res.json();
            })
                .subscribe(function (data) {
                var lastUpdate = new Date();
                _this.logger.debug(methodName, 'Data fetched for key ' + dataOptions.dataKey + ': ' + JSON.stringify(data));
                resolve({
                    lastUpdate: lastUpdate,
                    data: data
                });
            }, function (error) {
                _this.logger.error(methodName, 'Error fetching data for key ' + dataOptions.dataKey + ': ' + JSON.stringify(error));
                reject(error);
            });
        });
    };
    /**
     * Load data from local storage, and if the data is not present, refresh the data, update the data and then load it.
     * @param dataOptions the options for data service
     * @returns a Promise which resolves to the actual data loaded or the error
     */
    DataService.prototype.load = function (dataOptions) {
        var _this = this;
        var methodName = this.className + 'load';
        return new Promise(function (resolve, reject) {
            _this.storage.get(dataOptions.dataKey)
                .then(function (data) {
                if (data && data.lastUpdate) {
                    _this.logger.debug(methodName, 'Data for key ' + dataOptions.dataKey + ' has been loaded from the local storage.');
                    resolve(data);
                }
                else {
                    _this.logger.error(methodName, 'Error when loading the data for key: ' + dataOptions.dataKey + '. Obtaining from source...');
                    _this.loadFromRemote(dataOptions)
                        .then(function (data) {
                        resolve(data);
                    })
                        .catch(function (error) {
                        reject(error);
                    });
                }
            })
                .catch(function () {
                _this.logger.error(methodName, 'Error when loading the data for key: ' + dataOptions.dataKey + '. Obtaining from source...');
                _this.loadFromRemote(dataOptions)
                    .then(function (data) {
                    resolve(data);
                })
                    .catch(function (error) {
                    reject(error);
                });
            });
        });
    };
    /**
     * Force load data from remote, refresh the data, update the data and then load it.
     * @param dataOptions the options for data service
     * @returns a Promise which resolves to the actual data loaded or the error
     */
    DataService.prototype.loadFromRemote = function (dataOptions) {
        var _this = this;
        var methodName = this.className + 'loadFromRemote';
        return new Promise(function (resolve, reject) {
            _this.refresh(dataOptions)
                .then(function (data) {
                _this.update(dataOptions, data)
                    .then(function () {
                    _this.logger.debug(methodName, 'Data for key ' + dataOptions.dataKey + ' has been loaded from the original source.');
                    resolve(data);
                })
                    .catch(function (error) {
                    _this.logger.error(methodName, 'Error when loading the data for key: ' + dataOptions.dataKey);
                    reject(error);
                });
            });
        });
    };
    /**
     * Update local storage data for a particular key.
     * @param dataOptions the options for data service
     * @param value the value to be updated
     * @returns a Promise which resolves after successful update
     */
    DataService.prototype.update = function (dataOptions, value) {
        var _this = this;
        var methodName = this.className + 'update';
        return new Promise(function (resolve, reject) {
            _this.storage.set(dataOptions.dataKey, value)
                .then(function () {
                _this.logger.debug(methodName, 'Data for key ' + dataOptions.dataKey + ' has been updated.');
                resolve();
            })
                .catch(function (error) { return reject(error); });
        });
    };
    /**
     * Remove the data from local storage for a particular key.
     * @param dataOptions the options for data service
     * @returns a Promise which resolves after successful deletion
     */
    DataService.prototype.remove = function (dataOptions) {
        var _this = this;
        var methodName = this.className + 'delete';
        return new Promise(function (resolve, reject) {
            _this.storage.remove(dataOptions.dataKey)
                .then(function () {
                _this.logger.debug(methodName, 'Data for key ' + dataOptions.dataKey + ' has been deleted.');
            })
                .catch(function (error) { return reject(error); });
        });
    };
    /**
     * Reset is remove + refresh + update
     * @param dataOptions the options for data service
     * @returns a Promise which resolves after successful reset
     */
    DataService.prototype.reset = function (dataOptions) {
        var _this = this;
        var methodName = this.className + 'reset';
        return new Promise(function (resolve, reject) {
            _this.remove(dataOptions)
                .then(function () {
                _this.refresh(dataOptions)
                    .then(function (data) {
                    _this.update(dataOptions, data)
                        .then(function () {
                        _this.logger.debug(methodName, 'Data for key ' + dataOptions.dataKey + ' has been reset.');
                        resolve();
                    })
                        .catch(function (error) { return reject(error); });
                })
                    .catch(function (error) { return reject(error); });
            })
                .catch(function (error) { return reject(error); });
        });
    };
    /**
     * Checks whether there is local data in the local storage
     * @param dataOptions the options for data service
     * @returns true if there is data for the data key in the local storage, and false otherwise
     */
    DataService.prototype.isPresent = function (dataOptions) {
        var methodName = this.className + 'isPresent';
        var existedKey = lodash_1.default.find(this.storage.keys(), [dataOptions.dataKey]);
        for (var key in this.storage.keys()) {
            this.logger.debug(methodName, 'Key we have: ' + key);
        }
        this.logger.debug(methodName, 'Key found: ' + existedKey);
        return existedKey !== undefined && existedKey !== null;
    };
    DataService = __decorate([
        core_1.Injectable()
    ], DataService);
    return DataService;
}());
exports.DataService = DataService;
