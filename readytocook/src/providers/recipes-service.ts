import { Injectable } from '@angular/core';
import { Events } from 'ionic-angular';
import { LogService } from './log-service';
import { DataService } from './data-service';
import { DataOptions } from '../models/model-data-options';
import { Recipe } from '../models/model-recipe';
import lodash from 'lodash';
import 'rxjs/add/operator/map';

@Injectable()
export class RecipesService {
  private className: string = 'RecipesService -> ';

  private dataOptions: DataOptions = {
    dataKey: 'recipes'
  };

  public recipes: Array<Recipe>;
  public lastUpdate: Date;

  constructor(public logger: LogService,
              public dataService: DataService,
              public events: Events) {
    let methodName: string = this.className + 'constructor';
    this.logger.log(methodName, 'Welcome to recipes service');
  }

  init(): Promise<any> {
    let methodName: string = this.className + 'init';
    this.logger.log(methodName, 'initialising data...');
    return new Promise((resolve, reject) => {
      this.dataService.load(this.dataOptions)
        .then((data) => {
          this.logger.log(methodName, 'Data fresh from data service: ' + JSON.stringify(data));
          this.recipes = data.data;
          this.lastUpdate = data.lastUpdate;
          resolve();
        })
        .catch((error) => {
          this.logger.error(methodName, 'Error in refreshing data from data service: ' + JSON.stringify(error));
          this.recipes = [];
          this.lastUpdate = new Date();
          resolve();
        });
    });
  }

  // Add a new recipe or update the current recipe quantity by adding the new quantity
  addOrUpdateRecipe(recipe: Recipe) {
    let existingRecipe = lodash.find(this.recipes, ['name', recipe.name]);
    if (existingRecipe) {
      existingRecipe.name = recipe.name;
      existingRecipe.procedures = recipe.procedures;
      existingRecipe.ingredients = recipe.ingredients;
      if (recipe.image) {
        existingRecipe['image'] = recipe.image;
      }
    } else {
      this.recipes.push(recipe);
    }
    this.events.publish('data:updated', this.dataOptions, this.recipes);
  }

  // Remove an existing recipe
  removeRecipe(recipeName: string) {
    lodash.remove(this.recipes, (recipe) => {
      return recipe.name === recipeName;
    });
    this.events.publish('data:updated', this.dataOptions, this.recipes);
  }

  getRecipe(recipeName: string) {
    return lodash.find(this.recipes, ['name', recipeName]);
  }

}
