import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Storage } from '@ionic/storage';
import { Events } from 'ionic-angular';
import { ConfigService } from './config-service';
import { LogService } from './log-service';
import { DataOptions } from '../models/model-data-options';
import lodash from 'lodash';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/retryWhen';
import 'rxjs/add/operator/timeout';
import 'rxjs/add/operator/delay';

@Injectable()
export class DataService {

  private className = 'DataService -> ';

  constructor(public http: Http,
              public configService: ConfigService,
              public logger: LogService,
              public storage: Storage,
              public events: Events) {
    console.log('Hello DataService Provider');
  }

  /**
   * Load data from local storage, and if the data is not present, throw error.
   * @param dataOptions the options for data service
   * @returns a Promise which resolves to the actual data loaded or the error
   */
  load(dataOptions: DataOptions): Promise<any> {
    let methodName: string = this.className + 'load';
    return new Promise((resolve, reject) => {
      this.storage.get(dataOptions.dataKey)
        .then((data) => {
          if (data && data.lastUpdate) {
            this.logger.log(methodName, 'Data for key ' + dataOptions.dataKey + ' has been loaded from the local storage.');
            resolve(data);
          } else {
            this.logger.warn(methodName, 'Error when loading the data for key: ' + dataOptions.dataKey + '. Obtaining from source...');
            reject();
          }
        })
        .catch(() => {
          this.logger.warn(methodName, 'Error when loading the data for key: ' + dataOptions.dataKey + '. Obtaining from source...');
          reject();
        });
    });
  }

  /**
   * Update local storage data for a particular key.
   * @param dataOptions the options for data service
   * @param value the value to be updated
   * @returns a Promise which resolves after successful update
   */
  update(dataOptions: DataOptions, value: any): Promise<any> {
    let methodName: string = this.className + 'update';
    return new Promise((resolve, reject) => {
      let data = {
        data: value,
        lastUpdate: new Date()
      };
      this.storage.set(dataOptions.dataKey, data)
        .then(() => {
          this.logger.log(methodName, 'Data for key ' + dataOptions.dataKey + ' has been updated.');
          resolve();
        })
        .catch((error) => reject(error));
    });
  }


  /**
   * Remove the data from local storage for a particular key.
   * @param dataOptions the options for data service
   * @returns a Promise which resolves after successful deletion
   */
  remove(dataOptions: DataOptions): Promise<any> {
    let methodName: string = this.className + 'delete';
    return new Promise((resolve, reject) => {
      this.storage.remove(dataOptions.dataKey)
        .then(() => {
          this.logger.log(methodName, 'Data for key ' + dataOptions.dataKey + ' has been deleted.');
        })
        .catch((error) => reject(error));
    });
  }


  /**
   * Checks whether the app is running under preview mode or device mode
   * @returns true if the app is running under preview mode and false otherwise
   */
  isPreview(): boolean {
    let methodName: string = this.className + 'isPreview';
    this.logger.log(methodName, 'App running on hostname: ' + window.location.hostname);
    return window.location.hostname === 'localhost';
  }

}
