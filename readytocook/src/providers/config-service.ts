import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the ConfigService provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class ConfigService {

  public appConfig = {
    messages: {
      INFO_LOADING: 'App Initializing...',
      WARNING_INGREDIENT_DELETE: 'Do you want to remove this ingredient? It will also be removed from all related recipes.',
      ERROR_CONNECTION: 'There is an error fetching the data. Please check your Internet access or try again later.',
      ERROR_GENERIC: 'Ready to Cook app encounters an error.'
    }
  };

  public userConfig = {};

  constructor(public http: Http) {
    console.log('Hello ConfigService Provider');
  }

}
