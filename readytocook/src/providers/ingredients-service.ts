import { Injectable } from '@angular/core';
import { Events } from 'ionic-angular';
import { LogService } from './log-service';
import { DataService } from './data-service';
import { DataOptions } from '../models/model-data-options';
import { Ingredient } from '../models/model-ingredient';
import lodash from 'lodash';
import 'rxjs/add/operator/map';

@Injectable()
export class IngredientsService {
  private className: string = 'IngredientsService -> ';

  private dataOptions: DataOptions = {
    dataKey: 'ingredients'
  };

  public ingredients: Array<Ingredient>;
  public lastUpdate: Date;

  constructor(public logger: LogService,
              public dataService: DataService,
              public events: Events) {
    let methodName: string = this.className + 'constructor';
    this.logger.log(methodName, 'Welcome to ingredients service');
  }

  init(): Promise<any> {
    let methodName: string = this.className + 'init';
    this.logger.log(methodName, 'initialising data...');
    return new Promise((resolve, reject) => {
      this.dataService.load(this.dataOptions)
        .then((data) => {
          this.logger.log(methodName, 'Data fresh from data service: ' + JSON.stringify(data));
          this.ingredients = data.data;
          this.lastUpdate = data.lastUpdate;
          resolve();
        })
        .catch((error) => {
          this.logger.error(methodName, 'Error in refreshing data from data service: ' + JSON.stringify(error));
          this.ingredients = [];
          this.lastUpdate = new Date();
          resolve();
        });
    });
  }

  // Add a new ingredient or update the current ingredient quantity by adding the new quantity
  addIngredient(ingredient: Ingredient) {
    let existingIngredient = lodash.find(this.ingredients, ['name', ingredient.name]);
    if (existingIngredient) {
      existingIngredient.quantity += ingredient.quantity;
    } else {
      this.ingredients.push(ingredient);
    }
    this.events.publish('data:updated', this.dataOptions, this.ingredients);
  }

  // Update existing ingredient with new information
  updateIngredient(ingredient: Ingredient) {
    let existingIngredient = lodash.find(this.ingredients, ['name', ingredient.name]);
    if (existingIngredient) {
      existingIngredient.name = ingredient.name;
      existingIngredient.unit = ingredient.unit;
      existingIngredient.quantity = ingredient.quantity;
      if (ingredient.image) {
        existingIngredient['image'] = ingredient.image;
      }
    } else {
      this.ingredients.push(ingredient);
    }
    this.events.publish('data:updated', this.dataOptions, this.ingredients);
  }

  // Remove an existing ingredient
  removeIngredient(ingredientName: string) {
    lodash.remove(this.ingredients, (ingredient) => {
      return ingredient.name === ingredientName;
    });
    this.events.publish('data:updated', this.dataOptions, this.ingredients);
  }

  getIngredient(ingredientName: string) {
    return lodash.find(this.ingredients, ['name', ingredientName]);
  }
}
