import { Injectable } from '@angular/core';

@Injectable()
export class LogService {

  constructor() {}

  debug(prefix: string, message: string) {
    console.debug(prefix + ' -> ' + message);
  }

  log(prefix: string, message: string) {
    console.log(prefix + ' -> ' + message);
  }

  info(prefix: string, message: string) {
    console.info(prefix + ' -> ' + message);
  }

  warn(prefix: string, message: string) {
    console.warn(prefix + ' -> ' + message);
  }

  error(prefix: string, message: string) {
    console.error(prefix + ' -> ' + message);
  }

}
