import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Events } from 'ionic-angular';
import { LogService } from './log-service';
import 'rxjs/add/operator/map';


@Injectable()
export class AppService {

  private className: string = 'AppService -> ';

  isUpdating: boolean = false;

  constructor(public http: Http,
              public logger: LogService,
              public events: Events) {
    let methodName = this.className + 'constructor';
    this.isUpdating = false;
    this.logger.log(methodName, 'Hello AppService Provider');
  }
}
