"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var LogService = (function () {
    function LogService() {
    }
    LogService.prototype.debug = function (prefix, message) {
        console.debug(prefix + ' -> ' + message);
    };
    LogService.prototype.log = function (prefix, message) {
        console.log(prefix + ' -> ' + message);
    };
    LogService.prototype.info = function (prefix, message) {
        console.info(prefix + ' -> ' + message);
    };
    LogService.prototype.warn = function (prefix, message) {
        console.warn(prefix + ' -> ' + message);
    };
    LogService.prototype.error = function (prefix, message) {
        console.error(prefix + ' -> ' + message);
    };
    LogService = __decorate([
        core_1.Injectable()
    ], LogService);
    return LogService;
}());
exports.LogService = LogService;
