There are four major sections of this app.

One section is called "recipe". Here we define the recipe which has name, image (added later), procedures and ingredients.
One section is called "shelf". Here we have the list of ingredients we currently have with the name, quantity and unit. 
One section is called "cooking plan". Here we can select recipe from the recipe list and modify any content of it (which won't affect recipe list and the shelf list), and create a cooking plan. Each active recipe in the cooking plan will display the calculated ingredients (required/total). For calculation, the current plan will take into account all previous existing cooking plans as determined by creation date. 
One section is called "shopping list". For each of the cooking plan, we can generate a list of ingredient shopping list based on the calculation of current recipe list minus the required amount of the cooking plan. This list are then saved to the "shopping list" section. 
During the shopping, the user ticks the shopping list. When ticking the shopping list, the relavant stock is added to the "shelf". User can also skip this step to manually edit the inventory for each ingredient in the "shelf". 
After completing each recipe, tick it out, and then the inventory in "shelf" will reduce automatically. 

User can save the recipe in the cooking plan into the recipe list, either creating a new one or update an existing one. 